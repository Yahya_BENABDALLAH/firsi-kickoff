document.getElementById("animationContact").style.display = "none";
/// Submit Handler ///
function SubmitHandler() {
  document.getElementById("formContacter").style.display = "none";
  document.getElementById("animationContact").style.display = "flex";
  document.getElementById("animationContact").style.flexDirection = "column";
  Success.play();
}
/// contact animation ///
var Success = lottie.loadAnimation({
  container: document.getElementById("SuccessLottie"),
  renderer: "svg",
  loop: false,
  autoplay: false,
  name: "Success",
  path: "Success.json",
});
