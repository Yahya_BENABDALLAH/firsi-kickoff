document.addEventListener(
  "aos:in:F-stats",
  () => {
    $(".F-stats-count").each(function () {
      $(this)
        .prop("counter", 0)
        .animate(
          {
            counter: $(this).text(),
          },
          {
            duration: 4500,
            easing: "swing",
            step: function (now) {
              $(this).text(Math.ceil(now));
            },
          }
        );
    });
  },
  { once: true }
);
